Swy7ch' fork of dwm
===================

This is my fork of suckless' dynamic window manager, [dwm](https://dwm.suckless.org/). It is tweaked to match my needs and come with just a few patches:

* [dwm-fullgaps](https://dwm.suckless.org/patches/fullgaps/)
* [dwm-hide_vacant_tags](https://dwm.suckless.org/patches/hide_vacant_tags/)
* [dwm-swallow](https://dwm.suckless.org/patches/swallow/)

That's it!
----------

No need for anything else. I also changed the colors and the keybindings, along with setting particular tabs for particular apps. Enjoy!

---

dwm - dynamic window manager
============================
dwm is an extremely fast, small, and dynamic window manager for X.


Requirements
------------
In order to build dwm you need the Xlib header files.


Installation
------------
Edit config.mk to match your local setup (dwm is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install dwm (if
necessary as root):

    make clean install


Running dwm
-----------
Add the following line to your .xinitrc to start dwm using startx:

    exec dwm

In order to connect dwm to a specific display, make sure that
the DISPLAY environment variable is set correctly, e.g.:

    DISPLAY=foo.bar:1 exec dwm

(This will start dwm on display :1 of the host foo.bar.)

In order to display status info in the bar, you can do something
like this in your .xinitrc:

    while xsetroot -name "`date` `uptime | sed 's/.*,//'`"
    do
    	sleep 1
    done &
    exec dwm


Configuration
-------------
The configuration of dwm is done by creating a custom config.h
and (re)compiling the source code.
